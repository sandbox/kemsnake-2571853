-- About this module
This module allow create import file for Yandex.Realty system from your content.

-- How it works?
Module add new display called 'Yarealty'. You should enable this display on content type that you want export to Yandex.Realty.
After that you can copy example file node--examplenodetype--yrealty to your theme and rename {examplenodetype} to your content type name.
Change this new file as you need, i.e. field names, add/remove xml sections.
Run yarealty/export and get file. You can also use this url in Yandex.Realty settings as feed url.