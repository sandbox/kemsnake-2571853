<?php
/**
 * This is example temaplte for Yndex.Realty export
 *
 * Copy this file to you theme folder, rename for you content type and change field names in file.
 * You can add/delete/change xml tags value as you need
 *
 */

$wrapper = entity_metadata_wrapper('node', $node);
  $body = $wrapper->body->value();
?>

<offer internal-id="<?php print $node->nid; ?>">
  <manually-added>1</manually-added>
  <type><?php print $wrapper->field_sale_type->value()->name; ?></type>
  <property-type><?php print $wrapper->field_type->value()->name; ?></property-type>
  <category><?php print $wrapper->field_category->value()->name; ?></category>

  <url><?php print url('node/' . $node->nid, array('absolute'=>true)); ?></url>
  <creation-date><?php print date(DATE_W3C, $node->changed); ?></creation-date>

  <price>
    <value><?php print $wrapper->field_price->value(); ?></value>
    <currency><?php print $wrapper->field_currency->value(); ?></currency>
    <period><?php print $wrapper->field_adverttype->value(); ?></period>
  </price>

  <description><?php print $body['safe_value']; ?></description>

  <?php if ((int) $wrapper->field_squareall->value() > 0) :?>
    <area>
      <value><?php print $wrapper->field_squareall->value(); ?></value>
      <unit>кв.м</unit>
    </area>
  <?php endif; ?>
  <?php if ((int) $wrapper->field_squarelive->value() > 0) :?>
    <living-space>
      <value><?php print $wrapper->field_squarelive->value(); ?></value>
      <unit>кв.м</unit>
    </living-space>
  <?php endif; ?>
  <?php if ((int) $wrapper->field_squarecook->value() > 0) :?>
    <kitchen-space>
      <value><?php print $wrapper->field_squarecook->value(); ?></value>
      <unit>кв.м</unit>
    </kitchen-space>
  <?php endif; ?>

  <?php foreach ($wrapper->field_photo->value() as $image) {
      print '<image>' . file_create_url($image['uri']) . '</image>';
  }
  ?>

  <?php if ((int) $wrapper->field_room->value() > 0) :?>
    <rooms><?php print $wrapper->field_room->value(); ?></rooms>
  <?php endif; ?>

  <?php if ((int) $wrapper->field_floor->value() > 0) :?>
    <floor><?php print $wrapper->field_floor->value(); ?></floor  >
  <?php endif; ?>

  <?php if ((int) $wrapper->field_floorcount->value() > 0) :?>
    <floors-total><?php print $wrapper->field_floorcount->value(); ?></floors-total>
  <?php endif; ?>

  <?php if ((int) $wrapper->field_year->value()->name > 0) :?>
    <built-year><?php print (int) $wrapper->field_year->value()->name; ?></built-year>
  <?php endif; ?>
  <!--example for taxonmy reference field-->
  <building-type><?php print $wrapper->field_typewall->value()->name; ?></building-type>

  <location>
    <country>Россия</country>
    <locality-name><?php print $wrapper->field_city->value()->name; ?></locality-name>
    <sub-locality-name><?php print $wrapper->field_rayon->value()->name; ?></sub-locality-name>
    <address><?php print $wrapper->field_addres->value(); ?></address>
  </location>

  <sales-agent>
    <?php
      $phones = explode(',', $wrapper->field_phone->value());
      foreach ($phones as $phone) {
        print '<phone>' . trim($phone) . '</phone>';
      }
     ?>
    <?php if ((int) $wrapper->field_agent_name->value() !== '') :?>
      <name><?php print $wrapper->field_agent_name->value(); ?></name>
    <?php endif; ?>
  </sales-agent>
</offer>